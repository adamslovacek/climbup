package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.NetAdamsloClimbupArea;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NetAdamsloClimbupCountry
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-05T14:02:18.294Z")

public class NetAdamsloClimbupCountry   {
  @JsonProperty("ISO")
  private String ISO = null;

  @JsonProperty("LocalName")
  private String localName = null;

  @JsonProperty("GlobalName")
  private String globalName = null;

  @JsonProperty("Areas")
  private List<NetAdamsloClimbupArea> areas = null;

  public NetAdamsloClimbupCountry ISO(String ISO) {
    this.ISO = ISO;
    return this;
  }

   /**
   * Get ISO
   * @return ISO
  **/
  @ApiModelProperty(value = "")


  public String getISO() {
    return ISO;
  }

  public void setISO(String ISO) {
    this.ISO = ISO;
  }

  public NetAdamsloClimbupCountry localName(String localName) {
    this.localName = localName;
    return this;
  }

   /**
   * Get localName
   * @return localName
  **/
  @ApiModelProperty(value = "")


  public String getLocalName() {
    return localName;
  }

  public void setLocalName(String localName) {
    this.localName = localName;
  }

  public NetAdamsloClimbupCountry globalName(String globalName) {
    this.globalName = globalName;
    return this;
  }

   /**
   * Get globalName
   * @return globalName
  **/
  @ApiModelProperty(value = "")


  public String getGlobalName() {
    return globalName;
  }

  public void setGlobalName(String globalName) {
    this.globalName = globalName;
  }

  public NetAdamsloClimbupCountry areas(List<NetAdamsloClimbupArea> areas) {
    this.areas = areas;
    return this;
  }

  public NetAdamsloClimbupCountry addAreasItem(NetAdamsloClimbupArea areasItem) {
    if (this.areas == null) {
      this.areas = new ArrayList<NetAdamsloClimbupArea>();
    }
    this.areas.add(areasItem);
    return this;
  }

   /**
   * Get areas
   * @return areas
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<NetAdamsloClimbupArea> getAreas() {
    return areas;
  }

  public void setAreas(List<NetAdamsloClimbupArea> areas) {
    this.areas = areas;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NetAdamsloClimbupCountry netAdamsloClimbupCountry = (NetAdamsloClimbupCountry) o;
    return Objects.equals(this.ISO, netAdamsloClimbupCountry.ISO) &&
        Objects.equals(this.localName, netAdamsloClimbupCountry.localName) &&
        Objects.equals(this.globalName, netAdamsloClimbupCountry.globalName) &&
        Objects.equals(this.areas, netAdamsloClimbupCountry.areas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ISO, localName, globalName, areas);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NetAdamsloClimbupCountry {\n");
    
    sb.append("    ISO: ").append(toIndentedString(ISO)).append("\n");
    sb.append("    localName: ").append(toIndentedString(localName)).append("\n");
    sb.append("    globalName: ").append(toIndentedString(globalName)).append("\n");
    sb.append("    areas: ").append(toIndentedString(areas)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

