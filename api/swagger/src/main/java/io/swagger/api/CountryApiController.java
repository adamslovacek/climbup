package io.swagger.api;

import io.swagger.model.NetAdamsloClimbupCountry;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-05T14:02:18.294Z")

@Controller
public class CountryApiController implements CountryApi {



    public ResponseEntity<NetAdamsloClimbupCountry> countryISOGet(@ApiParam(value = "ISO 3166-1 alpha-2",required=true ) @PathVariable("ISO") String ISO) {
        // do some magic!
        return new ResponseEntity<NetAdamsloClimbupCountry>(HttpStatus.OK);
    }

}
